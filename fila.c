
#include<stdio.h>
#include<stdlib.h>


typedef struct elemento elem;
typedef struct lista ctrl_deque;

struct elemento
{
    struct nodo* raiz;

    elem *ant;
    elem *prox;
};

struct lista
{
    elem *ini;
    elem *fim;
    int tam;
};

void start_queue(ctrl_deque *deque);
void enqueue(ctrl_deque *deque, struct nodo* novo_elemento);
void ImprimeTodos(ctrl_deque *deque);
struct nodo*  dequeue(ctrl_deque *deque);
void destruir(ctrl_deque *deque);


void start_queue(ctrl_deque *deque)
{
    deque->ini=NULL;
    deque->fim=NULL;
    deque->tam=0;
}

void enqueue(ctrl_deque *deque, struct nodo* novo_elemento)
{

    elem *novo=NULL;
    novo = (elem*) malloc(sizeof(elem));
    if (novo == NULL) { exit(1); } // Falta de mem�ria

    novo->raiz = novo_elemento;

    if(deque->tam==0)
    { //Insrere na primeira posi��o
        novo->ant = deque->ini;
        novo->prox = deque->fim;
        deque->ini = novo;
        deque->fim = novo;
    }

    else
    { // Insere no Fim
        novo->prox = NULL;
        novo->ant = deque->fim;
        deque->fim->prox = novo;
        deque->fim = novo;
    }

    deque->tam++;
    //printf("colocou elem = %d\n", novo_elemento->valor);
}


struct nodo* dequeue(ctrl_deque *deque)
{
  // int i;
    elem *remov_elemento;
    struct nodo* aux;

    if(deque->ini == NULL && deque->fim == NULL)
        return NULL;

 // remove primeiro elemento
    remov_elemento = deque->ini;
    deque->ini = deque->ini->prox;

    if(deque->ini == NULL)
        deque->fim = NULL;

    else
        deque->ini->ant = NULL;

    aux = remov_elemento->raiz;
    free(remov_elemento); // libera memoria do elemento
    deque->tam--; // decrementa o contador de tamanho da deque
    //printf("Removeu elemento = %d\n", aux->valor);
    return aux;
}

void destruir(ctrl_deque *deque){

    while(deque->tam > 0) { // remove elemento por elemento
    dequeue(deque);
    }
}
