#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arvore.h"
#include "fila.c"

struct nodo * inicializa_arvore(int entradas, int * valores){
    if(entradas <= 0 || valores == NULL){
        return NULL;
    }
    int i;

    struct nodo* raiz = NULL;
    raiz = insere_nodo(raiz, valores[0]);
    for(i=1; i<entradas; i++){
        insere_nodo(raiz, valores[i]);
    }
    return raiz;
};

struct nodo * insere_nodo(struct nodo * raiz, int valor){
    if(raiz == NULL){
        struct nodo* raiz = (struct nodo*) malloc(sizeof(struct nodo));
            raiz->valor = valor;
            raiz->dir = NULL;
            raiz->esq = NULL;
        return raiz;
    }
    if(raiz->valor > valor){
        raiz->esq = insere_nodo(raiz->esq, valor);
        return raiz;
    }
    if(raiz->valor < valor){
        raiz->dir = insere_nodo(raiz->dir, valor);
        return raiz;
    }
    return NULL;
};

struct nodo* remove_nodo(struct nodo* raiz, int valor) {
    struct nodo* aux, *f_aux;

    if (raiz == NULL) {
        printf("Element doesnt exist in the tree.\n");
        return raiz;
    } else if (valor == raiz->valor) {
        if (raiz->esq == NULL && raiz->dir == NULL) {
            free(raiz);

            return NULL;
        } else if (raiz->esq != NULL && raiz->dir == NULL) {
            aux = raiz->esq;
            free(raiz);

            return aux;
        } else if (raiz->esq == NULL && raiz->dir != NULL) {
            aux = raiz->dir;
            free(raiz);

            return aux;
        } else {
            aux = raiz->dir;
            if (raiz->esq != NULL) {
                f_aux = raiz;

                while (aux->esq != NULL) {
                    f_aux = aux;
                    aux = aux->esq;
                }
                f_aux->esq = aux->dir;
                aux->esq = raiz->esq;
                aux->dir = raiz->dir;

                free(raiz);

                return aux;
            } else {
                aux->esq = raiz->esq;

                free(raiz);

                return aux;
            }
        }
    } else {
        if (valor > raiz->valor)
            raiz->dir = remove_nodo(raiz->dir, valor);
        else
            raiz->esq = remove_nodo(raiz->esq, valor);
    }
    return raiz;
}

/*
struct nodo * remove_nodo(struct nodo * raiz, int valor){
	struct nodo * filho;
	struct nodo * n_raiz;

	if (!raiz) return NULL;

	if (raiz->valor == valor) {
		if (raiz->dir) {
			n_raiz = filho = raiz->dir;

			while(filho->esq){
                filho = filho->esq;
			}
			filho->esq = raiz->esq;

			free (raiz);
			return n_raiz;

		} else {
			n_raiz = raiz->esq;
			free (raiz);
			return n_raiz;
		}

	} else if (valor > raiz->valor){
		raiz->dir = remove_nodo(raiz->dir, valor);
	}
	else{
        raiz->esq = remove_nodo(raiz->esq, valor);
	}


	return raiz;
}
*/


int altura(struct nodo * raiz){
    int esq, dir;
        if (raiz == NULL){
            return 0;
        }
            esq = altura(raiz->esq);
            dir = altura(raiz->dir);
        if (esq > dir){
            return esq+1;
        }
    else return dir+1;
}

struct nodo * busca(struct nodo * raiz, int valor){
	if (!raiz){
        return NULL;
	}
	if (raiz->valor == valor) {
			return raiz;
	} else if (valor > raiz->valor){
		raiz->dir = busca(raiz->dir, valor);
	}
	else{
        raiz->esq = busca(raiz->esq, valor);
	}

	return raiz;
}

int numero_elementos(struct nodo * raiz){
    int esq, dir;
        if (raiz == NULL){
            return 0;
        }
            esq = numero_elementos(raiz->esq);
            dir = numero_elementos(raiz->dir);
    return dir+esq+1;
}

int imprime(int * valores, int tamanho){
    int i;
    for(i=0; i < tamanho; i++){//for(i=-1*tamanho; i<0; i++){
        printf("%d ", valores[i]);
        if(i%10 == 0){
            printf("\n");
        }
    }
    return 0;
}

int balanceada(struct nodo * raiz){
    int esq, dir;
        if (raiz == NULL){
            return 0;
        }
            esq = numero_elementos(raiz->esq);
            dir = numero_elementos(raiz->dir);
    return dir-esq;
}

int postfix_internal(struct nodo * raiz, int *resultado, int* i){
    if(raiz == NULL){
        return 0;
    }
        postfix_internal(raiz->esq, resultado, i);
        postfix_internal(raiz->dir, resultado, i);
        resultado[*i] = raiz->valor;
        (*i)++;
        return numero_elementos(raiz);
}

int prefix_internal(struct nodo * raiz, int *resultado, int* i){
    if(raiz == NULL){
        return 0;
    }
        resultado[*i] = raiz->valor;
        (*i)++;
        prefix_internal(raiz->esq, resultado, i);
        prefix_internal(raiz->dir, resultado, i);
    return numero_elementos(raiz);
}

int infix_internal(struct nodo * raiz, int *resultado, int * i){
    if(raiz != NULL){
        infix_internal(raiz->esq, resultado, i);
        resultado[*i] = raiz->valor;
        (*i)++;
        //printf("i = %d, resultado[i] = %d\n", *i, resultado[*i]);
        infix_internal(raiz->dir, resultado, i);
    }
    return numero_elementos(raiz);
}

int prefix(struct nodo * raiz, int *resultado){
    int *i;
    int p = 0;
    i = &p;
    return prefix_internal(raiz, resultado, i);
}
int infix(struct nodo* raiz, int *resultado){
    int *i;
    int p = 0;
    i = &p;
    return infix_internal(raiz, resultado, i);
}
int postfix(struct nodo* raiz, int *resultado){
    int *i;
    int p = 0;
    i = &p;
    return postfix_internal(raiz, resultado, i);
}

void remove_todos(struct nodo * raiz)
{
    if (raiz != NULL)
    {   /// percorre a �rvore em P�s-ordem, liberando a mem�ria
        remove_todos(raiz->esq);
        remove_todos(raiz->dir);
        free(raiz);
    }
}

int abrangencia(struct nodo * raiz, int * resultado){

    ctrl_deque p;
    start_queue(&p);
    struct nodo * aux;
    int i = 0;
    if(raiz != NULL){
        enqueue(&p, raiz);
        while(p.tam > 0){
            aux = dequeue(&p);
            resultado[i++] = aux->valor;
            if(aux->esq != NULL){
                enqueue(&p, aux->esq);
            }
            if(aux->dir != NULL){
                enqueue(&p, aux->dir);
            }
        }
    }
    return numero_elementos(raiz);
}
